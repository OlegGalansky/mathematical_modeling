const func = x => x - Math.cos(x)

const range = (start, stop, step) =>  {
    var result = [];
    for (var i = start; step > 0 ? i < stop : i > stop; i += step) {
        result.push(i);
    }
    return result;
};
const checkInterval = (inter1 = null, inter2 = null, f) => {
    return ((f(inter1) > 0) && (f(inter2) > 0)) || ((f(inter1) < 0) && (f(inter2) < 0))
}
const integrate = (func, first, second, n) => {
        let integral = 0
        let step = (second - first) / n
        for (let x in range(first, second-step, step)){
            integral += step*(func(x) + func(x + step)) / 2
            console.log(integral)
        }    
        return integral
}

const integrate2 = (func, first, second, n) => {
        let integral = 0
        let step = (second - first) / n
        for (let x in range(first + step / 2, second - step / 2, step)){
            integral += step / 6 * (func(x - step / 2) + 4 * func(x) + func(x + step / 2))
            console.log(integral)
        }
        return integral
}

const  trapeziumMethod = (func, first, second, delta) => {
    let d = 1
    let n = 1
    while (Math.abs(d) > delta){
        d = (integrate(func, first, second, n * 2) - integrate(func, first, second, n)) / 3
        n *= 2
    }
    a = Math.abs(integrate(func, first, second, n))
    b = Math.abs(integrate(func, first, second, n)) + d
    if (a > b) [a, b] = [b, a]
    console.log(n, a, b)
}

const simpsonMethod = (func, first, second, delta) => {
    let d = 1
    let n = 1
    while (Math.abs(d) > delta){
        d = (integrate2(func, first, second, n * 2) - integrate2(func, first, second, n)) / 15
        n *= 2
    }

    a = Math.abs(integrate2(func, first, second, n))
    b = Math.abs(integrate2(func, first, second, n)) + d
    if (a > b) [a, b] = [b, a]
    console.log(n, a, b)
}    


const method = function(){
    const specificMethod = prompt("Выберите метод")
    if (specificMethod === '1') return trapeziumMethod

    if (specificMethod === '2') return simpsonMethod

}()

let firstInterval
let secondInterval

for (; checkInterval(firstInterval,secondInterval,func) === true;){
    firstInterval = +prompt("Введите новый промежуток")
    secondInterval = +prompt("Введите новый промежуток")
}

const accuracy = +prompt("Введите точность")
const numOfcharacters = accuracy.toString().length - 1

method(func,firstInterval,secondInterval,accuracy)

