const func = x => x - Math.cos(x)
const deriv = x => Math.sin(x) + 1
const secDeriv = x => Math.cos(x)
const checkInterval = (inter1 = null, inter2 = null, f) => {
    return ((f(inter1) > 0) && (f(inter2) > 0)) || ((f(inter1) < 0) && (f(inter2) < 0))
}

let methodFlag = false
let firstInterval
let secondInterval
let prev   

const method = function(){
    const specificMethod = prompt("Выберите метод")
    if (specificMethod === '1') return x => x - func(x) / deriv(x)

    if (specificMethod === '2') {
        return x => x - func(x) * deriv(x) / 
        (Math.pow(deriv(x), 2) - 0.5 * secDeriv(x) * func(x))

    }

    if (specificMethod === '3'){
        methodFlag = true
        return (...value) =>  {
            value[0]- (func(value[0]) * (value[0] - value[1])) /
         (func(value[0]) - func(value[1]))
        }
    }

}()

for (; checkInterval(firstInterval,secondInterval,func);){
    firstInterval = +prompt("Введите новый промежуток")
    secondInterval = +prompt("Введите новый промежуток")
}

const accuracy = +prompt("Введите точность")
const numOfcharacters = accuracy.toString().length - 1

let current = Math.max(firstInterval, secondInterval)
let next = method(current)

if (methodFlag){
    prev = Math.max(firstInterval,secondInterval)
    current = Math.min(firstInterval,secondInterval)
}

document.write(`Начальное значение: ${current} <br>`)

document.write(`Первое приближение: ${next} <br>`)


for (var counter = 0; Math.abs(current - next) > accuracy; counter++){
    prev = current
    current = next
    next = method(current, prev)
    document.write(`Приближение ${counter} : ${next} <br>`)
}
document.write(`Количество приближений ${counter} <br>`)





